package repository;

import model.Author;
import util.HibernateUtil;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class AuthorRepository {

    private EntityManager entityManager = HibernateUtil.getSessionFactory().createEntityManager();

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void saveAuthor(Author author) {
        entityManager.getTransaction().begin();
        entityManager.merge(author);
        entityManager.getTransaction().commit();
    }

    public void updateAuthorById(Author newAuthor) {
        entityManager.getTransaction().begin();
        Author author = entityManager.getReference(Author.class, newAuthor.getId());
        author.setName(newAuthor.getName());
        author.setBooks(newAuthor.getBooks());
        entityManager.persist(author);
        entityManager.getTransaction().commit();
    }

    public void removeAuthorById(int id) {
        entityManager.getTransaction().begin();
        Author author = entityManager.getReference(Author.class, id);
        entityManager.remove(author);
        entityManager.getTransaction().commit();
    }

    public Optional<Author> getAuthorById(int id) {
        return Optional.ofNullable(entityManager.find(Author.class, id));
    }

    public List<Author> getAllAuthors(){
        List<Author> authors = entityManager.createQuery("SELECT a FROM Author a").getResultList();
        return authors == null ? Collections.emptyList() : authors;
    }
}
