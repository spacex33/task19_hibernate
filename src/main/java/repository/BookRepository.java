package repository;

import model.Book;
import util.HibernateUtil;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class BookRepository {

    private EntityManager entityManager = HibernateUtil.getSessionFactory().createEntityManager();

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void saveBook(Book book) {
        entityManager.getTransaction().begin();

        // If your entity is new, it's the same as a persist().
        // But if your entity already exists, it will update it.
        entityManager.merge(book);

        entityManager.getTransaction().commit();
    }

    public void updateBookById(Book newBook) {
        entityManager.getTransaction().begin();
        Book book = entityManager.getReference(Book.class, newBook.getId());
        book.setTitle(newBook.getTitle());
        book.setDescription(newBook.getDescription());
        book.setImageUrl(newBook.getImageUrl());
        book.setReleaseDate(newBook.getReleaseDate());
        book.setAuthors(newBook.getAuthors());
        book.setReviews(newBook.getReviews());
        entityManager.persist(book);
        entityManager.getTransaction().commit();
    }

    public void removeBookById(int id) {
        entityManager.getTransaction().begin();
        Book book = entityManager.getReference(Book.class, id);
        entityManager.remove(book);
        entityManager.getTransaction().commit();
    }

    public Optional<Book> getBookById(int id) {
        return Optional.ofNullable(entityManager.find(Book.class, id));
    }

    public List<Book> getAllBooks(){
        List<Book> books = entityManager.createQuery("SELECT b FROM Book b").getResultList();
        return books == null ? Collections.emptyList() : books;
    }
}
