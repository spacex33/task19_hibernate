package repository;

import model.Review;
import util.HibernateUtil;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class ReviewRepository {

    private EntityManager entityManager = HibernateUtil.getSessionFactory().createEntityManager();

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void saveReview(Review review) {
        entityManager.getTransaction().begin();
        entityManager.merge(review);
        entityManager.getTransaction().commit();
    }

    public void updateReviewById(Review newReview) {
        entityManager.getTransaction().begin();
        Review review = entityManager.getReference(Review.class, newReview.getId());
        review.setBook(newReview.getBook());
        review.setComment(newReview.getComment());
        review.setStarsNum(newReview.getStarsNum());
        entityManager.persist(review);
        entityManager.getTransaction().commit();
    }

    public void removeReviewById(int id) {
        entityManager.getTransaction().begin();
        Review review = entityManager.getReference(Review.class, id);
        entityManager.remove(review);
        entityManager.getTransaction().commit();
    }

    public Optional<Review> getReviewById(int id) {
        return Optional.ofNullable(entityManager.find(Review.class, id));
    }

    public List<Review> getAllReviews(){
        List<Review> reviews = entityManager.createQuery("SELECT r FROM Review r").getResultList();
        return reviews == null ? Collections.emptyList() : reviews;
    }

}
