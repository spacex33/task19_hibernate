package service;

import model.Author;
import repository.AuthorRepository;

import java.util.List;

public class AuthorService {

    private AuthorRepository authorRepository;

    public AuthorService(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    public List<Author> findAll() {
        return authorRepository.getAllAuthors();
    }

    public Author findById(int id) {
        return authorRepository.getAuthorById(id).orElseThrow(NullPointerException::new);
    }

    public void save(Author author) {
        authorRepository.saveAuthor(author);
    }

    public void update(Author author) {
        authorRepository.updateAuthorById(author);
    }

    public void delete(int id) {
        authorRepository.removeAuthorById(id);
    }
}
