package service;

import model.Book;
import repository.BookRepository;

import java.util.List;

public class BookService {

    private BookRepository bookRepository;

    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public List<Book> findAll() {
        return bookRepository.getAllBooks();
    }

    public Book findById(int id) {
        return bookRepository.getBookById(id).orElseThrow(NullPointerException::new);
    }

    public void save(Book book) {
        bookRepository.saveBook(book);
    }

    public void update(Book book) {
        bookRepository.updateBookById(book);
    }

    public void delete(int id) {
        bookRepository.removeBookById(id);
    }
}
