package service;

import model.Review;
import repository.ReviewRepository;

import java.util.List;

public class ReviewService {

    private ReviewRepository reviewRepository;

    public ReviewService(ReviewRepository reviewRepository) {
        this.reviewRepository = reviewRepository;
    }

    public List<Review> findAll() {
        return reviewRepository.getAllReviews();
    }

    public Review findById(int id) {
        return reviewRepository.getReviewById(id).orElseThrow(NullPointerException::new);
    }

    public void save(Review review) {
        reviewRepository.saveReview(review);
    }

    public void update(Review review) {
        reviewRepository.updateReviewById(review);
    }

    public void delete(int id) {
        reviewRepository.removeReviewById(id);
    }
}
