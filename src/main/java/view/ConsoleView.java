package view;

import model.Book;
import repository.AuthorRepository;
import repository.BookRepository;
import repository.ReviewRepository;
import service.AuthorService;
import service.BookService;
import service.ReviewService;
import util.HibernateUtil;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ConsoleView {

    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Scanner input = new Scanner(System.in);
    private BookService bookService = new BookService(new BookRepository());
    private AuthorService authorService = new AuthorService(new AuthorRepository());
    private ReviewService reviewService = new ReviewService(new ReviewRepository());

    public ConsoleView() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        menu.put("1", "   1 - select All Books");
        methodsMenu.put("1", this::selectAllBooks);
        menu.put("2", "   2 - find Book By Id");
        methodsMenu.put("2", this::findBookById);
        menu.put("3", "   3 - create Book");
        methodsMenu.put("3", this::createBook);
        menu.put("4", "   4 - update Book");
        methodsMenu.put("4", this::updateBook);
        menu.put("5", "   5 - delete Book");
        methodsMenu.put("5", this::deleteBook);

    }

    // book

    private void createBook() {
        System.out.println("Input title for Book: ");
        String title = input.nextLine();
        System.out.println("Input description for Book: ");
        String description = input.nextLine();
        System.out.println("Input image_url for Book: ");
        String image_url = input.nextLine();
        System.out.println("Input release_date for Book: ");
        String release_date = input.nextLine();
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = formatter.parse(release_date);
            Book book = new Book(title, description, image_url, date);
            bookService.save(book);
            System.out.println("added: " + book);
        } catch (ParseException e) {
            System.out.println("Input error, try again");
        }
    }

    private void selectAllBooks() {
        List<Book> books = bookService.findAll();
        for (Book book : books) {
            System.out.println(book);
        }
    }

    private void findBookById() {
        System.out.println("Input ID(id) for Book: ");
        int id = Integer.valueOf(input.nextLine());
        try {
            Book book = bookService.findById(id);
            System.out.println(book);
        } catch (NullPointerException e) {
            System.out.println("Book with such id not found");
        }
    }

    private void updateBook() {
        System.out.println("Input title for Book: ");
        String title = input.nextLine();
        System.out.println("Input description for Book: ");
        String description = input.nextLine();
        System.out.println("Input image_url for Book: ");
        String image_url = input.nextLine();
        System.out.println("Input release_date for Book: ");
        String release_date = input.nextLine();
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = formatter.parse(release_date);
            Book book = new Book(title, description, image_url, date);
            bookService.update(book);
            System.out.println("updated: " + book);
        } catch (ParseException e) {
            System.out.println("Update error, try again");
        }
    }

    private void deleteBook() {
        System.out.println("Input id for Book: ");
        String id = input.nextLine();
        bookService.delete(Integer.valueOf(id));
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String key : menu.keySet())
            if (key.length() == 1) System.out.println(menu.get(key));
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();

            if (keyMenu.matches("^\\d")) {
                methodsMenu.get(keyMenu).print();
            }
        } while (!keyMenu.equals("Q"));
        HibernateUtil.getSessionFactory().createEntityManager().getEntityManagerFactory().close();
    }

}
