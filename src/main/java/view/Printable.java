package view;

public interface Printable {
    void print();
}
