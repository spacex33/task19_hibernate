CREATE SCHEMA IF NOT EXISTS `book_store` DEFAULT CHARACTER SET utf8 ;
USE `book_store` ;

-- -----------------------------------------------------
-- Table `book_store`.`book`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `book_store`.`book` (
  `id` INT NOT NULL,
  `title` VARCHAR(45) NOT NULL,
  `description` VARCHAR(300) NULL,
  `image_url` VARCHAR(100) NULL,
  `release_date` DATE NULL,
  PRIMARY KEY (`id`))

-- -----------------------------------------------------
-- Table `book_store`.`author`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `book_store`.`author` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))

-- -----------------------------------------------------
-- Table `book_store`.`review`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `book_store`.`review` (
  `id` INT NOT NULL,
  `stars_num` INT NOT NULL,
  `comment` VARCHAR(500) NULL,
  `book_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_review_book1_idx` (`book_id` ASC),
  CONSTRAINT `fk_review_book1`
    FOREIGN KEY (`book_id`)
    REFERENCES `book_store`.`book` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)

-- -----------------------------------------------------
-- Table `book_store`.`book_has_author`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `book_store`.`book_has_author` (
  `book_id` INT NOT NULL,
  `author_id` INT NOT NULL,
  PRIMARY KEY (`book_id`, `author_id`),
  INDEX `fk_book_has_author_author1_idx` (`author_id` ASC),
  INDEX `fk_book_has_author_book1_idx` (`book_id` ASC),
  CONSTRAINT `fk_book_has_author_book1`
    FOREIGN KEY (`book_id`)
    REFERENCES `book_store`.`book` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_book_has_author_author1`
    FOREIGN KEY (`author_id`)
    REFERENCES `book_store`.`author` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)

insert into author values(1, 'Stephen King');
insert into author values(2, 'Owen King');
insert into author values(3, 'George R. R. Martin');

insert into book values(1, 'Elevation', 'The latest from legendary master storyteller Stephen King, a riveting, extraordinarily eerie, and moving story about a man whose mysterious affliction brings a small town together', 'https://images-na.ssl-images-amazon.com/images/I/51%2BVJLKPxqL._SX353_BO1,204,203,200_.jpg', '2018-10-30');
insert into book values(2, 'Sleeping Beauties: A Novel', 'In this spectacular father/son collaboration, Stephen King and Owen King tell the highest of high-stakes stories', 'https://images-na.ssl-images-amazon.com/images/I/41lpXkCIrCL._SX342_BO1,204,203,200_.jpg', '2017-09-26');
insert into book values(3, 'A Game of Thrones: A Song of Ice and Fire', 'Winter is coming.', 'https://images-na.ssl-images-amazon.com/images/I/51n5SAiAz7L._SX342_.jpg', '2003-12-09');

insert into book_has_author values(1, 1);
insert into book_has_author values(2, 1);
insert into book_has_author values(2, 2);
insert into book_has_author values(3, 3);

insert into review values(1, 4, null, 2);
insert into review values(2, 5, null, 3);

ALTER TABLE book_store.author MODIFY id INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE book_store.book MODIFY id INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE book_store.review MODIFY id INT(11) NOT NULL AUTO_INCREMENT;

